# Formation EDF du 27/06/2019

Utilisation de l'[API IKATS](https://github.com/IKATS/ikats_api#ikats-python-api) via un notebook Jupyter instancié sur le cluster.

**Liens :**

- Principes de l'API : [github.com/IKATS/ikats_api/README.md](https://github.com/IKATS/ikats_api#ikats-python-api)
- Notebook tutoriel pour l'utilisation de l'API : https://raw.githubusercontent.com/IKATS/ikats_api/master/Tutorial.ipynb  
  > Une copie de ce fichier est disponible dans ce dépôt


## Utilisation de ce dépôt pour la formation

1. Ouvrez l'url suivante : http://jupyter.edf.intra.ikats.org
   - Choisissez un nom de login et un password, le notebook Jupyter sera créé et acessible avec ces identifiants
2. Ouvrez un nouveau terminal `New > Terminal`  
   ![](screenshots/jupyter_new_terminal.png)
3. Dans la console exécuter la commande suivante :  
   `git clone https://gitlab.com/ikats/formation-edf.git`
   ![](screenshots/jupyter_terminal_clone.png)
   Vous pouvez fermer l'onglet du terminal pour revenir à la liste des fichiers de votre Jupyter.
